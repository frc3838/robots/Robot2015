package frc.team3838.controls;

import java.lang.reflect.Constructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.robot2015.commands.DeactivatedCommandStandInCommand;
import frc.team3838.robot2015.commands.NoOpCommand;
import frc.team3838.robot2015.commands.autonomous.InitializeRobotForRecycleBinCommandGroup;
import frc.team3838.robot2015.commands.autonomous.InitializeRobotForToteCommandGroup;
import frc.team3838.robot2015.commands.claw.ClawCloseCommand;
import frc.team3838.robot2015.commands.claw.ClawOpenCommand;
import frc.team3838.robot2015.commands.claw.ClawToggleCommand;
import frc.team3838.robot2015.commands.winch.WinchSpeedLimitOverrideOffCommand;
import frc.team3838.robot2015.commands.winch.WinchSpeedLimitOverrideOnCommand;



public class Joysticks
{
    private static final Logger logger = LoggerFactory.getLogger(Joysticks.class);


    private Joysticks() { }


    public static final JoystickDefinition<Extreme3DJoystickConfig> DRIVER = new JoystickDefinition<>(Extreme3DJoystickConfig.class, "DRIVER", 0);
    public static final JoystickDefinition<Attack3JoystickConfig> OPS_1 = new JoystickDefinition<>(Attack3JoystickConfig.class, "OPS_1", 1);


    // ==== BUTTON ASSIGNMENTS ===/


    private static final NoOpCommand noOpCommand = new NoOpCommand();
    private static final Class<? extends Command> no_cmd_assigned = NoOpCommand.class;


    // Static initializer
    static {initButtons();}

    /** Initializes the joysticks and the joystick button assignments. * */
    public static void init() {initButtons();}


    /**
     * Assigns command to a joystick button by its number (1-11).
     *
     * <pre>
     *        JOYSTICK BUTTON LAYOUT
     * ***************************************
     * *             (1 Trigger)             *
     * *          .................          *
     * *          :               :          *
     * * +---+    :     +---+     :    +---+ *
     * * | 6 |    . +-+ | 3 | +-+ :    + 11| *
     * * +---+    : |4| +---+ |5| :    +---+ *
     * *          : +-+ +---+ +-+ :          *
     * * +---+    :     | 2 |     :    +---+ *
     * * | 7 |    :     +---+     :    | 10| *
     * * +---+    :...............:    +---+ *
     * * Left          Stick           Right *
     * * Group      +---+  +---+       Group *
     * *            | 8 |  | 9 |             *
     * *            +---+  +---+             *
     * *            Bottom Group             *
     * ***************************************
     * </pre>
     */
    private static void initButtons()
    {

        // @formatter:off

        /* 1*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.TRIGGER_1,           ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 2*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.SIDE_THUMB_2,        ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 3*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_LEFT_LOWER_3,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 4*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_LOWER_4, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 5*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_LEFT_UPPER_5,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 6*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_UPPER_6, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 7*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_UPPER_RIGHT_7,  ButtonAction.WHEN_PRESSED, cmd(InitializeRobotForToteCommandGroup.class));
        /* 8*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_UPPER_LEFT_8,   ButtonAction.WHEN_PRESSED, cmd(InitializeRobotForRecycleBinCommandGroup.class));
        /* 9*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_RIGHT_9, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /*10*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_LEFT_10, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /*11*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_RIGHT_11, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /*12*/ Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_LEFT_12,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /*11*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_RIGHT_11, ButtonAction.WHEN_PRESSED, cmd(WinchResetMinMaxReadingsCommand.class));
        /*12*/ //Joysticks.DRIVER.assignButton(Extreme3DJoystickConfig.BASE_LOWER_LEFT_12,  ButtonAction.WHEN_PRESSED, cmd(WinchEncoderResetCommand.class));



        /* 1*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED, cmd(ClawCloseCommand.class));
        /* 1*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_RELEASED, cmd(ClawOpenCommand.class));
        /* 2*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED, cmd(WinchSpeedLimitOverrideOnCommand.class));
        /* 2*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_RELEASED, cmd(WinchSpeedLimitOverrideOffCommand.class));
        /* 3*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED, cmd(ClawToggleCommand.class));
        /* 4*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED, cmd(ClawOpenCommand.class));
        /* 5*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED, cmd(ClawCloseCommand.class));
        /* 6*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 7*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 8*/ //Joysticks.OPS_1.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED, cmd(WinchEncoderResetCommand.class));
        /* 8*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /* 9*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /*10*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));
        /*11*/ Joysticks.OPS_1.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));




        /* 1*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 2*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 3*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 4*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 5*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 6*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 7*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 8*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 9*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*10*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*11*/ /*Joysticks.OPS_2.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/



        /* 1*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.TRIGGER_1,           ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 2*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.SIDE_THUMB_2,        ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 3*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_LEFT_LOWER_3,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 4*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_LOWER_4, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 5*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_LEFT_UPPER_5,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 6*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.STICK_RIGHT_UPPER_6, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 7*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_UPPER_RIGHT_7,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 8*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_UPPER_LEFT_8,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 9*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_RIGHT_9, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*10*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_MIDDLE_LEFT_10, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*11*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_LOWER_RIGHT_11, ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*11*/ /*Joysticks.FOO.assignButton(Extreme3DJoystickConfig.BASE_LOWER_LEFT_12,  ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/

        /* 1*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.TRIGGER_1,              ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 2*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_BOTTOM_2,      ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 3*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_TOP_3,         ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 4*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_LEFT_4,        ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 5*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.HIGH_HAT_RIGHT_5,       ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 6*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.LEFT_GROUP_UPPER_6,     ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 7*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.LEFT_GROUP_LOWER_7,     ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 8*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_LEFT_8,    ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /* 9*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.BOTTOM_GROUP_RIGHT_9,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*10*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.RIGHT_GROUP_LOWER_10,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/
        /*11*/ /*Joysticks.FOO.assignButton(Attack3JoystickConfig.RIGHT_GROUP_UPPER_11,   ButtonAction.WHEN_PRESSED, cmd(no_cmd_assigned));*/



        // @formatter:on
    }


    private static Command cmd(Class<? extends Command> commandClass, Object... parameters)
    {

        if (commandClass == null)
        {
            return noOpCommand;
        }

        logger.debug("cmd method creating command instance for {}", commandClass.getName());
        if (NoOpCommand.class.isAssignableFrom(commandClass))
        {
            logger.debug("cmd method received the NoOpCommand class. Returning the static NoOpCommand instance.");
            return noOpCommand;
        }
        try
        {
            logger.debug("Attempting to reflectively create an instance of {}", commandClass.getName());

            if (parameters == null || parameters.length == 0)
            {
                return commandClass.newInstance();
            }
            else
            {
                Class<?>[] parameterTypes = new Class[parameters.length];
                for (int i = 0; i < parameters.length; i++)
                {
                    parameterTypes[i] = parameters[i].getClass();
                }
                final Constructor<? extends Command> constructor = commandClass.getConstructor(parameterTypes);
                return constructor.newInstance(parameters);
            }


        }
        catch (Exception e)
        {
            logger.warn("Could not create an instance of the {}. Cause details: {}", commandClass.getSimpleName(), e.toString(), e);
            return new DeactivatedCommandStandInCommand(commandClass);
        }
    }


}
