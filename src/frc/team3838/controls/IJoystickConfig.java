package frc.team3838.controls;

public interface IJoystickConfig
{
    int getButtonNumber();

    String getJoystickTypeName();
}
