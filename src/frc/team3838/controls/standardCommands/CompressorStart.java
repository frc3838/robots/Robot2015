package frc.team3838.controls.standardCommands;

import frc.team3838.robot2015.commands.CommandBase;



/**
 * Start the compressor running in closed loop control mode
 * Use the method in cases where you would like to manually stop and start the compressor
 * for applications such as conserving battery or making sure that the compressor motor
 * doesn't start during critical operations.
 * <p><strong style="color:red">This command does not need to be used during normal operation.
 * It is available mainly for testing and for stopping/starting the compressor during long practice
 * runs tro prevent it from overheating.</strong></p>
 */
public class CompressorStart extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CompressorStart.class);


    public CompressorStart()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(clawSubsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            // No op
        }
        else
        {
            logger.info("Not all required subsystems for {} are enabled. The command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                clawSubsystem.startCompressor();
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (clawSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
