package frc.team3838.utils.enums;

public interface RobotQuadrant
{
    String getName();
}
