package frc.team3838.robot2015.subsystems;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.robot2015.EM;
import frc.team3838.robot2015.commands.winch.WinchMotorRunCommand;
import frc.team3838.robot2015.config.DIO;
import frc.team3838.robot2015.config.PWM;
import frc.team3838.robot2015.config.SDBKeys;
import frc.team3838.utils.MathUtils;

import static frc.team3838.utils.LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE;



@SuppressWarnings("UnusedDeclaration")
public class WinchSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(WinchSubsystem.class);


    public static final double UP_SPEED_MAX = 0.6;
    private static final double UP_SPEED_SLOW_DOWN = 0.4;
    public static final double MAX_UP_HEIGHT = 78.6;  //was 84.5
    private static final double UP_SLOW_DOWN_HEIGHT = MAX_UP_HEIGHT - 12.0; //Slow down for the last x inches

    public static final double MIN_DOWN_HEIGHT = 0;

    public  static final double DOWN_SPEED_MAX = -0.50;
    private static final double DOWN_SPEED_SLOW_DOWN = -0.25;
    private static final double DOWN_SPEED_EXTRA_SLOW_DOWN = -0.1;

    private static final double DOWN_SLOW_DOWN_HEIGHT = 14; //Slow down for the last 14 inches
    private static final double DOWN_EXTRA_SLOW_DOWN_HEIGHT = 1.5; //Really Slow down for the last little bit (in inches)
    public  static final double LOWER_SAFETY_CUTOFF = -0.05;

    private SpeedController speedController;
    private Encoder encoder;

    private DigitalInput lowerLimitSwitch;
    private DigitalInput upperLimitSwitch;
    public static final int PULSES_PER_REVOLUTION = 64;

    private boolean speedLimitOverridden = false;

    private boolean initialZeroingResetHasOccurred = false;

    private double maxHeight = -10000;
    private double minHeight = 10000;

    private double maxSpeed = -1;

    /* MUST BE LAT FIELD IN CLASS */
    private static final WinchSubsystem singleton = new WinchSubsystem();

    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new WinchSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     WinchSubsystem subsystem = new WinchSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     WinchSubsystem subsystem = WinchSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static WinchSubsystem getInstance() {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     WinchSubsystem subsystem = new WinchSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     WinchSubsystem subsystem = WinchSubsystem.getInstance();
     * </pre>
     */
    private WinchSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());

                speedController = new Jaguar(PWM.WINCH_MOTOR);
                encoder = new Encoder(DIO.WINCH_ENCODER_CH_A, DIO.WINCH_ENCODER_CH_B);
                //Circumference in ft =  diameterInInches * PI  =   4 in / 12 (in / ft) * PI
                encoder.setDistancePerPulse((3.0 * Math.PI) / PULSES_PER_REVOLUTION);
                encoder.reset();

                lowerLimitSwitch = new DigitalInput(DIO.WINCH_LOWER_LIMIT_SW);
                upperLimitSwitch = new DigitalInput(DIO.WINCH_UPPER_LIMIT_SW);

                logger.debug("{} initialization completed successfully", getClass().getSimpleName());

            }
            catch (Exception e)
            {
                EM.isWinchSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.info("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }

    @Override
    public void initDefaultCommand()
    {
        setDefaultCommand(new WinchMotorRunCommand());
    }


    public void run(GenericHID hid)
    {
        //Without conversion, a positive speed is up, a negative speed is down
        double speed = hid.getY();
        run(speed);
    }


    public void run(double speed)
    {
        final double currentHeight = getHeightInInches();

        if (MathUtils.isPositive(speed))
        {
            //we are trying to go up and we are past the limit switch
            if (isUpperLimitSwitch() || (initialZeroingResetHasOccurred && currentHeight >= MAX_UP_HEIGHT))
            {
                speed = 0;
            }
            else if (currentHeight >= UP_SLOW_DOWN_HEIGHT && !speedLimitOverridden)
            {
                speed = Math.min(speed, UP_SPEED_SLOW_DOWN);
            }
            else
            {
                speed = Math.min(speed, UP_SPEED_MAX);
            }
        }
        else if (MathUtils.isNegative(speed))
        {
            if (isLowerLimitSwitch())
            {
                speed = 0;
                reset();
                initialZeroingResetHasOccurred = true;
            }
            else if (isPastLowerSafetyHeightLimit())
            {
                //A backup in the event the limit switch breaks we want to prevent thw winch from unwinding
                speed = 0;
            }
            else if(currentHeight <= DOWN_SLOW_DOWN_HEIGHT)
            {
                speed = Math.max(speed, DOWN_SPEED_SLOW_DOWN);
            }
            else if (currentHeight <= DOWN_EXTRA_SLOW_DOWN_HEIGHT)
            {
                speed = DOWN_SPEED_EXTRA_SLOW_DOWN;
            }
            else
            {
                speed = Math.max(speed, DOWN_SPEED_MAX);
            }
        }
        logger.trace("Sending speed of {} to speed controller", speed);
        speedController.set(speed);
        sdbPutHeight();
        sdbPutSpeed(speed);
    }


    public boolean isPastLowerSafetyHeightLimit() {return initialZeroingResetHasOccurred && getHeightInInches() <= LOWER_SAFETY_CUTOFF;}

    public boolean isAtLowerLimit()
    {
        return isLowerLimitSwitch() || isPastLowerSafetyHeightLimit();
    }

    public void stop()
    {
        if (EM.isWinchSubsystemEnabled)
        {
            logger.debug("WinchSubsystem.stop() called");
            speedController.set(0.0);
        }
    }


    public boolean isRunning()
    {
        return EM.isWinchSubsystemEnabled && speedController.get() != 0;
    }

    public void logEncoderStatus()
    {
        logger.debug("Encoder.getDistance = {} inches", encoder.getDistance());
    }


    public double getHeightInFeet()
    {
        return encoder.getDistance() / 12;
    }


    public double getHeightInInches()
    {
        return encoder.getDistance();
    }

    public void reset()
    {
        encoder.reset();
    }

    public void init()
    {
        reset();
        initialZeroingResetHasOccurred = true;
    }

    public void sdbPutHeight()
    {
        final double height = MathUtils.roundToNPlaces(getHeightInInches(), 3);
        logger.trace("{}: {}", SDBKeys.ELEVATOR_HEIGHT, height);
        SmartDashboard.putNumber(SDBKeys.ELEVATOR_HEIGHT, height);

        maxHeight = MathUtils.roundToNPlaces(Math.max(maxHeight, height), 3);
        minHeight = MathUtils.roundToNPlaces(Math.min(minHeight, height), 3);

        if (IN_DEBUGGING_MODE)
        {
            SmartDashboard.putNumber(SDBKeys.ELEVATOR_HEIGHT_MAX, maxHeight);
            SmartDashboard.putNumber(SDBKeys.ELEVATOR_HEIGHT_MIN, minHeight);
            logger.trace("WWinch Height: {}   Min: {}   Max: {}", height, minHeight, maxHeight);
        }
    }

    public void resetMinMaxHeightReadings()
    {
        maxHeight = -10000;
        minHeight = 10000;
    }

    public void sdbPutSpeed(double speed)
    {
        SmartDashboard.putNumber(SDBKeys.WINCH_SPEED, speed);
    }

    public boolean isEnabled()
    {
        return EM.isWinchSubsystemEnabled;
    }

    public boolean isLowerLimitSwitch() { return !lowerLimitSwitch.get(); }

    public boolean isUpperLimitSwitch() { return !upperLimitSwitch.get(); }

    public void raise()
    {
        run(UP_SPEED_MAX);
    }

    public void raise(double speed)
    {
        run(MathUtils.makePositive(speed));
    }

    public void lower()
    {
        run(DOWN_SPEED_MAX);
    }

    public void lower(double speed)
    {
        run(MathUtils.makeNegative(speed));
    }

    public void putLimitSwitchValues()
    {
        SmartDashboard.putBoolean(SDBKeys.WINCH_LOWER_LIMIT_SWITCH, isLowerLimitSwitch());
        SmartDashboard.putBoolean(SDBKeys.WINCH_LOWER_LIMIT_SWITCH_Alt, !isLowerLimitSwitch());
        SmartDashboard.putBoolean(SDBKeys.WINCH_UPPER_LIMIT_SWITCH, isUpperLimitSwitch());
        SmartDashboard.putBoolean(SDBKeys.WINCH_UPPER_LIMIT_SWITCH_Alt, !isUpperLimitSwitch());
    }


    public void setSpeedLimitOverridden(boolean speedLimitOverridden)
    {
        this.speedLimitOverridden = speedLimitOverridden;
    }


    public boolean isSpeedLimitOverridden()
    {
        return speedLimitOverridden;
    }
}
