package frc.team3838.robot2015.subsystems.drive;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Gyro;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.RobotDrive.MotorType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.robot2015.EM;
import frc.team3838.robot2015.commands.driveTrain.DriveViaMecanumCommand;
import frc.team3838.robot2015.config.AIO;
import frc.team3838.robot2015.config.DIO;
import frc.team3838.robot2015.config.PWM;
import frc.team3838.robot2015.config.SDBKeys;
import frc.team3838.utils.MathUtils;

import static frc.team3838.utils.LogbackProgrammaticConfiguration.IN_DEBUGGING_MODE;



@SuppressWarnings("UnusedDeclaration")
public class DriveTrainSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(DriveTrainSubsystem.class);

    private static final boolean invertFrontLeft = false;
    private static final boolean invertFrontRight = false;
    private static final boolean invertRearLeft = false;
    private static final boolean invertRearRight = false;


    private Wheel frontLeftWheel;
    private Wheel frontRightWheel;
    private Wheel rearLeftWheel;
    private Wheel rearRightWheel;
    private RobotDrive robotDrive;

    private Gyro gyro;

    @SuppressWarnings("FieldCanBeLocal")
    private ImmutableMap<MotorType, Wheel> wheelMap;


    private static final DriveTrainSubsystem singleton = new DriveTrainSubsystem();
    private boolean useCurve = false;
    private int curvePower = 3;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new DriveTrainSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static DriveTrainSubsystem getInstance() {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     */
    private DriveTrainSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.info("Initializing {}", getClass().getSimpleName());

                // @formatter:off
                frontLeftWheel =  new Wheel(MotorType.kFrontLeft,  PWM.FRONT_LEFT__WHEEL,  DIO.FRONT_LEFT__WHEEL_ENCODER_CH_A,  DIO.FRONT_LEFT__WHEEL_ENCODER_CH_B);
                frontRightWheel = new Wheel(MotorType.kFrontRight, PWM.FRONT_RIGHT_WHEEL,  DIO.FRONT_RIGHT_WHEEL_ENCODER_CH_A,  DIO.FRONT_RIGHT_WHEEL_ENCODER_CH_B);
                rearLeftWheel =   new Wheel(MotorType.kRearLeft,   PWM.REAR__LEFT__WHEEL,  DIO.REAR__LEFT__WHEEL_ENCODER_CH_A,  DIO.REAR__LEFT__WHEEL_ENCODER_CH_B);
                rearRightWheel =  new Wheel(MotorType.kRearRight,  PWM.REAR__RIGHT_WHEEL,  DIO.REAR__RIGHT_WHEEL_ENCODER_CH_A,  DIO.REAR__RIGHT_WHEEL_ENCODER_CH_B);
                // @formatter:on

                final Builder<MotorType, Wheel> builder = ImmutableMap.builder();
                builder.put(MotorType.kFrontLeft, frontLeftWheel);
                builder.put(MotorType.kFrontRight, frontRightWheel);
                builder.put(MotorType.kRearLeft, rearLeftWheel);
                builder.put(MotorType.kRearRight, rearRightWheel);
                wheelMap = builder.build();

                robotDrive = new RobotDrive(frontLeftWheel.getSpeedController(),
                                            rearLeftWheel.getSpeedController(),
                                            frontRightWheel.getSpeedController(),
                                            rearRightWheel.getSpeedController());

                robotDrive.setExpiration(1);

                robotDrive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, invertFrontLeft);
                robotDrive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, invertFrontRight);
                robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, invertRearLeft);
                robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, invertRearRight);

                try
                {
                    gyro = new Gyro(AIO.GYRO);
                    logger.info("Initializing {} Gyro", getClass().getSimpleName());
                    gyro.initGyro();
                }
                catch (Exception e)
                {
                    logger.error("COULD NOT INITIALIZE GYRO. Cause: {}",e.toString(), e);
                }

                //Reset puts distances on Smart Dashboard
                reset();

                logger.info("{} initialization completed successfully", getClass().getSimpleName());

            }
            catch (Exception e)
            {
                EM.isDriveTrainSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.info("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }



    public void setUseCurve(boolean useCurve)
    {
        this.useCurve = useCurve;
    }

    @Override
    public void initDefaultCommand()
    {
        setDefaultCommand(new DriveViaMecanumCommand());
    }

    public void driveViaMecanum(GenericHID hid)
    {
        driveViaMecanum(hid.getX(), hid.getY(), hid.getTwist(), 0);
    }

    public void driveViaMecanum(double x, double y, double zOrTwist)
    {
        driveViaMecanum(x,y, zOrTwist,0);
    }

    public void driveViaMecanum(double x, double y, double zOrTwist, double gyroAngle)
    {
        if (useCurve)
        {
            x = curveAdjustValue(x);
            y = curveAdjustValue(y);
            zOrTwist = curveAdjustValue(zOrTwist);
        }

        robotDrive.mecanumDrive_Cartesian(x, y, zOrTwist, gyroAngle);
        sdbPutDistances();
        sdbPurJoyStickValues(x, y, zOrTwist);
    }

    protected double curveAdjustValue(double v)
    {
        final boolean wasNegative = MathUtils.isNegative(v);
        final double curveValue = Math.abs(Math.pow(v, curvePower));
        return wasNegative ? -curveValue : curveValue;
    }


    /**
     *
     * @param rotationDirection the direction of rotation
     * @param speed speed of rotation between 0 and 1; recommended to keep is slow; about 0.50 is a good speed
     */
    public void rotate(RotationDirection rotationDirection, double speed)
    {
        //CW is pos; CCW is neg
        speed = (rotationDirection == RotationDirection.CLOCKWISE) ? MathUtils.makePositive(speed) : MathUtils.makeNegative(speed);
        logger.debug("Rotating {} at a speed of {}", rotationDirection, speed);
        robotDrive.mecanumDrive_Cartesian(0, 0, speed, 0);
        sdbPutDistances();
    }

    /**
     *
     * @param speed The speed from 0 to 1
     */
    public void driveForward(double speed)
    {
//        robotDrive.mecanumDrive_Cartesian(speed, 0, 0, 0);

        logger.info("setting speed to {}%", speed);
        frontLeftWheel.setSpeed(speed);
        frontRightWheel.setSpeed(speed);
        rearLeftWheel.setSpeed(speed);
        rearRightWheel.setSpeed(speed);

        sdbPutDistances();
    }


    /**
     * @param speed The speed from 0 to 1
     */
    public void driveLeft(double speed)
    {
        logger.info("setting speed to {}%", speed);
        robotDrive.mecanumDrive_Cartesian(speed, 0, 0, 0);


//        frontLeftWheel.setSpeed(speed);
//        frontRightWheel.setSpeed(speed);
//        rearLeftWheel.setSpeed(speed);
//        rearRightWheel.setSpeed(speed);

        sdbPutDistances();
    }


    public void drive(Direction direction, double speed)
    {
        speed = Math.abs(speed);
        final double x;
        final double y;
        final double rotation;
        switch (direction)
        {
            case FORWARD:
                x = 0;
                y = -speed;
                rotation = 0;
                break;
            case REVERSE:
                x = 0;
                y = speed;
                rotation = 0;
                break;
            case LEFT:
                x = -speed;
                y = 0;
                rotation = 0;
                break;
            case RIGHT:
                x = speed;
                y = 0;
                rotation = 0;
                break;
            default:
                x = 0;
                y = 0;
                rotation = 0;
        }
        robotDrive.mecanumDrive_Cartesian(x, y, rotation, 0);
        sdbPutDistances();
    }


    public void driveAdvanced(Direction direction, double speed)
    {
        speed = Math.abs(speed);
        final double x;
        final double y;
        final double rotation;
        switch (direction)
        {
            case FORWARD:
                x = 0;
                y = -speed;
                rotation = 0;
                break;
            case REVERSE:
                x = 0;
                y = speed;
                rotation = 0;
                break;
            case LEFT:
                x = -speed;
                y = 0;
                rotation = 0;
                break;
            case RIGHT:
                x = speed;
                y = 0;
                rotation = 0;
                break;
            default:
                x = 0;
                y = 0;
                rotation = 0;
        }

        @SuppressWarnings("UnnecessaryLocalVariable")
        double xIn = x;
        double yIn = y;
        // Negate y for the joystick.
        yIn = -yIn;

        double wheelSpeeds[] = new double[4];
        wheelSpeeds[frontLeftWheel.getIndex()] = xIn + yIn + rotation;
        wheelSpeeds[frontRightWheel.getIndex()] = -xIn + yIn - rotation;
        wheelSpeeds[rearLeftWheel.getIndex()] = -xIn + yIn + rotation;
        wheelSpeeds[rearRightWheel.getIndex()] = xIn + yIn - rotation;

        normalize(wheelSpeeds);

        frontLeftWheel.setSpeed(wheelSpeeds[frontLeftWheel.getIndex()]);
        frontRightWheel.setSpeed(wheelSpeeds[frontRightWheel.getIndex()]);
        rearLeftWheel.setSpeed(wheelSpeeds[rearLeftWheel.getIndex()]);
        rearRightWheel.setSpeed(wheelSpeeds[rearRightWheel.getIndex()]);
    }


    /**
     * Rotate a vector in Cartesian space.
     */
    protected static double[] rotateVector(double x, double y, double angle)
    {
        double cosA = Math.cos(angle * (3.14159 / 180.0));
        double sinA = Math.sin(angle * (3.14159 / 180.0));
        double out[] = new double[2];
        out[0] = x * cosA - y * sinA;
        out[1] = x * sinA + y * cosA;
        return out;
    }


    /**
     * Normalize all wheel speeds if the magnitude of any wheel is greater than 1.0.
     */
    protected static void normalize(double wheelSpeeds[])
    {
        double maxMagnitude = Math.abs(wheelSpeeds[0]);
        int i;
        for (i = 1; i < 4; i++)
        {
            double temp = Math.abs(wheelSpeeds[i]);
            if (maxMagnitude < temp) maxMagnitude = temp;
        }
        if (maxMagnitude > 1.0)
        {
            for (i = 0; i < 4; i++)
            {
                wheelSpeeds[i] = wheelSpeeds[i] / maxMagnitude;
            }
        }
    }


    public double getGyroAngle()
    {
        return gyro != null ? MathUtils.roundToNPlaces(gyro.getAngle(), 2) : 0;
    }



    public void stop()
    {
        robotDrive.stopMotor();
    }


    public double getAverageDistanceInInches()
    {
        double total = frontLeftWheel.getDistanceInInches() +
                       frontRightWheel.getDistanceInInches() +
                       rearLeftWheel.getDistanceInInches() +
                       rearRightWheel.getDistanceInInches();

        return total / 4;
    }

    public double getRightDistanceInInches()
    {
        return ((frontLeftWheel.getDistanceInInches() + rearRightWheel.getDistanceInInches()) - (frontRightWheel.getDistanceInInches() - rearLeftWheel.getDistanceInInches())) / 4;
    }


    public double getLeftDistanceInInches()
    {
        return ((frontRightWheel.getDistanceInInches() + rearLeftWheel.getDistanceInInches()) - (frontLeftWheel.getDistanceInInches() - rearRightWheel.getDistanceInInches())) / 4;
    }

    public double getSideMovementDistance()
    {
        double total = Math.abs(frontLeftWheel.getDistanceInInches()) +
                       Math.abs(frontRightWheel.getDistanceInInches()) +
                       Math.abs(rearLeftWheel.getDistanceInInches()) +
                       Math.abs(rearRightWheel.getDistanceInInches());

        return total / 8;
    }


    public double getAverageDistanceInInchesRounded()
    {
        return MathUtils.roundToNPlaces(getAverageDistanceInInches(), 3) ;
    }

    public String getAverageDistanceInInchesAsString()
    {
        return MathUtils.formatNumber(getAverageDistanceInInches(), 3) ;
    }

    public double getAverageDistanceInFeet()
    {
        double total = frontLeftWheel.getDistanceInFeet() +
                       frontRightWheel.getDistanceInFeet() +
                       rearLeftWheel.getDistanceInFeet() +
                       rearRightWheel.getDistanceInFeet();

        return total / 4;
    }

    public void disableMotorSafetyForAutonomousMode()
    {
        robotDrive.setSafetyEnabled(false);
    }

    public void enableMotorSafetyPostAutonomousMode()
    {
        robotDrive.setSafetyEnabled(true);
    }

    public double getAverageDistanceInFeetRounded()
    {
        return MathUtils.roundToNPlaces(getAverageDistanceInFeet(), 3) ;
    }


    public boolean isEnabled()
    {
        return EM.isDriveTrainSubsystemEnabled;
    }

    public void reset()
    {
        frontLeftWheel.resetEncoder();
        frontRightWheel.resetEncoder();
        rearLeftWheel.resetEncoder();
        rearRightWheel.resetEncoder();
        sdbPutDistances();
    }

    public void resetGyro() { gyro.reset(); }

    public void initGyro() { gyro.initGyro(); }


    private void sdbPutDistances()
    {
        sdbPutGyroAngle();
        sdbPutAverageDistance();
        sdbPutIndividualWheelDistances();
        sdbPutSideDistance();
    }

    private void sdbPurJoyStickValues(GenericHID hid)
    {
        sdbPurJoyStickValues(hid.getX(), hid.getY(), hid.getZ(), hid.getTwist());
    }


    private void sdbPurJoyStickValues(double x, double y, double z)
    {
        sdbPurJoyStickValues(x, y, z, z);
    }

    private void sdbPurJoyStickValues(double x, double y, double z, double t)
    {
        if (IN_DEBUGGING_MODE)
        {
            SmartDashboard.putNumber("X", MathUtils.roundToNPlaces(x, 3));
            SmartDashboard.putNumber("Y", MathUtils.roundToNPlaces(y, 3));
            SmartDashboard.putNumber("Z", MathUtils.roundToNPlaces(z, 3));
            SmartDashboard.putNumber("T", MathUtils.roundToNPlaces(t, 3));

            if (logger.isTraceEnabled())
            {
                String fmt = String.format("X: %6s  Y: %6s  Z: %6s  T: %6s",
                              MathUtils.formatNumber(x, 3),
                              MathUtils.formatNumber(y, 3),
                              MathUtils.formatNumber(z, 3),
                              MathUtils.formatNumber(t, 3)
                );
                logger.trace("X: {}   Y: {}   Z: {}   T: {}", x, y, z, t);
            }
        }
    }


    public void sdbPutIndividualWheelDistances()
    {
        if (IN_DEBUGGING_MODE)
        {
            final double flDistance = frontLeftWheel.getDistanceInInchesRounded();
            final double frDistance = frontRightWheel.getDistanceInInchesRounded();
            final double rlDistance = rearLeftWheel.getDistanceInInchesRounded();
            final double rrDistance = rearRightWheel.getDistanceInInchesRounded();


            SmartDashboard.putNumber(SDBKeys.FRONT_LEFT__DISTANCE, flDistance);
            SmartDashboard.putNumber(SDBKeys.FRONT_RIGHT_DISTANCE, frDistance);
            SmartDashboard.putNumber(SDBKeys.REAR__LEFT__DISTANCE, rlDistance);
            SmartDashboard.putNumber(SDBKeys.REAR__RIGHT_DISTANCE, rrDistance);



            if (logger.isTraceEnabled())
            {
                logger.trace("FL: {}  FR: {}  RL: {}  RR: {}", flDistance, frDistance, rlDistance, rrDistance);
            }
        }
    }
    public void sdbPutGyroAngle()
    {
        SmartDashboard.putNumber(SDBKeys.GYRO_ANGLE, getGyroAngle());
    }

    public void sdbPutAverageDistance()
    {
        if (IN_DEBUGGING_MODE)
        {
            final double avgDistance = getAverageDistanceInInches();
            logger.trace("Average Distance: {}", avgDistance);
            SmartDashboard.putNumber(SDBKeys.AVG_DISTANCE, avgDistance);
        }
    }


    public void sdbPutSideDistance()
    {
        if (IN_DEBUGGING_MODE)
        {
            SmartDashboard.putNumber(SDBKeys.LEFT_DISTANCE, getLeftDistanceInInches());
            SmartDashboard.putNumber(SDBKeys.RIGHT_DISTANCE, getRightDistanceInInches());
            SmartDashboard.putNumber(SDBKeys.SIDE_MOVEMENT_DISTANCE, getSideMovementDistance());
        }
    }

    public int spinPower()
    {
        curvePower++;
        if (curvePower > 4)
        {
            curvePower = 1;
        }

        SmartDashboard.putNumber(SDBKeys.DRIVE_CURVE, curvePower);

        return curvePower;
    }

    public static enum Direction
    {
        FORWARD,
        REVERSE,
        LEFT,
        RIGHT
    }

    public static enum RotationDirection { CLOCKWISE
        {
            @Override
            public String toString()
            {
                return "CW";
            }
        },
        COUNTERCLOCKWISE
        {
            @Override
            public String toString()
            {
                return "CCW";
            }
        }
    }
}
