package frc.team3838.robot2015.commands.testComponents;

import frc.team3838.robot2015.commands.CommandBase;



public class LogCmdSequenceCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LogCmdSequenceCommand.class);

    private int executeCount = 0;
    private int runCount = 1;


    public LogCmdSequenceCommand()
    {

    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        logger.info("~~~ {}.initialize Called         - run # {}", getClass().getSimpleName(), runCount);
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        executeCount++;

        logger.info("~~~~~ {}.execute Called         - execute count {} of run {}", getClass().getSimpleName(), executeCount, runCount);
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        logger.info("~~~~~ {}.isFinished() called     - execute count {} of run {}", getClass().getSimpleName(), executeCount, runCount);
        return executeCount >= 4;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.info("~~~~ {}.end() called             - execute count {} of run {}", getClass().getSimpleName(), executeCount, runCount);
        runCount++;
        executeCount = 0;

    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {
        logger.info("~~~~ {}.interrupted() called     - execute count {} of run {}", getClass().getSimpleName(), executeCount, runCount);
    }
}
