package frc.team3838.robot2015.commands.winch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.robot2015.commands.CommandBase;



public class WinchEncoderResetCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(WinchEncoderResetCommand.class);
    
    public WinchEncoderResetCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(winchSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
//        if (allSubsystemsAreEnabled())
//        {
//            // No op
//        }
//        else
//        {
//            logger.info("Not all required subsystems for WinchEncoderResetCommand are enabled. The command can not be and will not be initialized or executed.");
//        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        logger.debug("WinchEncoderResetCommand.execute() called");
        if (allSubsystemsAreEnabled())
        {
            try
            {
                winchSubsystem.reset();
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in WinchEncoderResetCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for WinchEncoderResetCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (winchSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
