package frc.team3838.robot2015.commands.winch;

import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.subsystems.WinchSubsystem;



public class WinchLowerCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WinchLowerCommand.class);

    private boolean isFirstCall = true;

    private final double inchesToLower;

    private double targetHeight;


    public static WinchLowerCommand feet(double feetToLower)
    {
        return new WinchLowerCommand(feetToLower * 12);
    }

    public static WinchLowerCommand inches(double inchesToLower)
    {
        return new WinchLowerCommand(inchesToLower);
    }

    private WinchLowerCommand(double inchesToLower)
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(winchSubsystem);
        this.inchesToLower = inchesToLower;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            //no op
        }
        else
        {
            logger.info("Not all required subsystems for WinchLowerCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                if (isFirstCall)
                {
                    final double currentHeight = winchSubsystem.getHeightInInches();
                    final double targetHeight = winchSubsystem.getHeightInInches() - inchesToLower;
                    this.targetHeight = Math.max(targetHeight, WinchSubsystem.MIN_DOWN_HEIGHT);
                    logger.debug("{} first execution with a requested delta of {} inches. Current Height = {}; Target Height calculated as {}",
                                 getClass().getSimpleName(), inchesToLower, currentHeight, this.targetHeight);
                    isFirstCall = false;
                }

                if (isFinished())
                {
                    winchSubsystem.stop();
                }
                else
                {
                    final double remainingDistance = winchSubsystem.getHeightInInches() - targetHeight;
                    final double speed;
                    if (remainingDistance > 8)
                    {
                        speed = WinchSubsystem.DOWN_SPEED_MAX;
                    }
                    else if (remainingDistance < 2)
                    {
                        speed = -0.25;
                    }
                    else
                    {
                        speed = -0.4;
                    }

                    winchSubsystem.lower(speed);
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in WinchLowerCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for WinchLowerCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return winchSubsystem.isAtLowerLimit() || winchSubsystem.getHeightInInches() <= targetHeight;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (winchSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.debug("{}.end() called", getClass().getSimpleName());
        winchSubsystem.stop();
        //get ready for the next use
        isFirstCall = true;
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
