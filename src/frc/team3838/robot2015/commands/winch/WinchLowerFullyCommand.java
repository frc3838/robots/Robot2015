package frc.team3838.robot2015.commands.winch;

import frc.team3838.robot2015.commands.CommandBase;



public class WinchLowerFullyCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WinchLowerFullyCommand.class);


    public WinchLowerFullyCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(winchSubsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            //no op
        }
        else
        {
            logger.info("Not all required subsystems for WinchLowerCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                if (isFinished())
                {
                    winchSubsystem.stop();
                }
                else
                {
                    winchSubsystem.lower();
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in WinchLowerCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for WinchLowerCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        final boolean  finished;
        if (winchSubsystem.isLowerLimitSwitch())
        {
            finished = true;
            winchSubsystem.init();
        }
        else
        {
            finished = winchSubsystem.isPastLowerSafetyHeightLimit();
        }
        return finished;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (winchSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        winchSubsystem.stop();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
