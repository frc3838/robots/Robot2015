package frc.team3838.robot2015.commands.driveTrain.drive;

import edu.wpi.first.wpilibj.Timer;
import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.subsystems.drive.DriveTrainSubsystem.Direction;



public class DriveSetTime2 extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveSetTime2.class);

    private final Direction direction;
    private double seconds;

    public static DriveSetTime2 forwardSeconds(double seconds) { return new DriveSetTime2(Direction.FORWARD, seconds); }
    public static DriveSetTime2 reverseSeconds(double seconds) { return new DriveSetTime2(Direction.REVERSE, seconds); }
    public static DriveSetTime2 leftSeconds(double seconds) { return new DriveSetTime2(Direction.LEFT, seconds); }
    public static DriveSetTime2 rightSeconds(double seconds) { return new DriveSetTime2(Direction.RIGHT, seconds); }
    public static DriveSetTime2 seconds(Direction direction, double seconds) { return new DriveSetTime2(direction, seconds); }

    public DriveSetTime2(Direction direction, double seconds)
    {
        this.seconds = seconds;
        requires(driveTrainSubsystem);
        this.direction = direction;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {

    }



    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                double speed = 1.0;
                driveTrainSubsystem.drive(direction, speed);
                Timer.delay(seconds);
                driveTrainSubsystem.stop();
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in DriveSetDistance.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for DriveSetDistance are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        driveTrainSubsystem.stop();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
