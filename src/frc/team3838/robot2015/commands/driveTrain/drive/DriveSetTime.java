package frc.team3838.robot2015.commands.driveTrain.drive;

import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.subsystems.drive.DriveTrainSubsystem.Direction;



public class DriveSetTime extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveSetTime.class);

    private final Direction direction;
    private long startTime;
    private long ms;
    private boolean finished = false;
    private boolean isFirstCall = true;

    public static DriveSetTime forwardSeconds(double seconds) { return new DriveSetTime(Direction.FORWARD, (long) (seconds * 1000)); }
    public static DriveSetTime reverseSeconds(double seconds) { return new DriveSetTime(Direction.REVERSE, (long) (seconds * 1000)); }
    public static DriveSetTime leftSeconds(double seconds) { return new DriveSetTime(Direction.LEFT, (long) (seconds * 1000)); }
    public static DriveSetTime rightSeconds(double seconds) { return new DriveSetTime(Direction.RIGHT, (long) (seconds * 1000)); }
    public static DriveSetTime seconds(Direction direction, double seconds) { return new DriveSetTime(direction, (long) (seconds * 1000)); }

    public DriveSetTime(Direction direction, long ms)
    {
        this.ms = ms;
        requires(driveTrainSubsystem);
        this.direction = direction;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {

    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                if (isFirstCall)
                {
                    startTimer();
                    isFirstCall = false;
                    logger.debug("Executing {} for {} ms",getClass().getSimpleName(), ms);
                }
                long diff = System.currentTimeMillis() - startTime;
                finished = diff >= ms;
                if (finished)
                {
                    logger.debug("{} Stopping after {}ms", getClass().getSimpleName(), ms);
                }
                else
                {
                    double speed = 1.0;
                    driveTrainSubsystem.drive(direction, speed);
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in DriveSetDistance.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for DriveSetDistance are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        if (finished)
        {
            logger.debug("{} of {} ms has completes", getClass().getSimpleName(), ms);
        }
        return finished;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        driveTrainSubsystem.stop();
        //reset for the next run
        isFirstCall = true;
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
