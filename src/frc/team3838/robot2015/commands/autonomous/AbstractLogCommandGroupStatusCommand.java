package frc.team3838.robot2015.commands.autonomous;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.robot2015.commands.CommandBase;



public abstract class AbstractLogCommandGroupStatusCommand extends CommandBase
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final Command commandToLog;

    protected static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    protected LocalTime statusTime;

    protected AbstractLogCommandGroupStatusCommand(Command commandToLog) {this.commandToLog = commandToLog;}


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        statusTime = LocalTime.now();
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        logger.info("~~~~~ {} {} command at {}{}", getStatus(), commandToLog.getName(), timeFormatter.format(statusTime), getAdditionalMessage());
    }


    protected abstract String getStatus();


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }


    public LocalTime getStatusTime()
    {
        return statusTime;
    }

    protected String getAdditionalMessage()
    {
        return "";
    }
}
