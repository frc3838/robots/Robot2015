package frc.team3838.robot2015.commands.autonomous;


import frc.team3838.robot2015.commands.winch.WinchRaiseCommand;



public class InitializeRobotForRecycleBinCommandGroup extends InitializeRobotForToteCommandGroup
{


    public InitializeRobotForRecycleBinCommandGroup()
    {
        super();
    }


    public InitializeRobotForRecycleBinCommandGroup(String name)
    {
        super(name);
    }


    @Override
    protected void addCommands()
    {
        super.addCommands();
        addSequential(WinchRaiseCommand.inches(14.0));
    }

}