package frc.team3838.robot2015.commands.autonomous;


import org.joda.time.DateTime;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team3838.controls.standardCommands.SleepCommand;
import frc.team3838.robot2015.commands.claw.ClawCloseCommand;
import frc.team3838.robot2015.commands.claw.ClawOpenCommand;
import frc.team3838.robot2015.commands.winch.WinchLowerFullyCommand;
import frc.team3838.robot2015.config.MatchDates;

import static frc.team3838.controls.standardCommands.SleepCommand.PartialSeconds.NoPartial;



public class InitializeRobotForToteCommandGroup extends CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InitializeRobotForToteCommandGroup.class);

    private boolean isFirstCall = true;

    public InitializeRobotForToteCommandGroup()
    {
        super();
        addCommands();
    }


    public InitializeRobotForToteCommandGroup(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {


        final DateTime now = DateTime.now();
        if (MatchDates.isDuringMatch(now))
        {
            addSequential(new ClawCloseCommand());
        }
        else
        {
            addSequential(new ClawOpenCommand());
            addSequential(SleepCommand.createSeconds(2, NoPartial));
        }
        addSequential(new WinchLowerFullyCommand());
        addSequential(SleepCommand.createSeconds(1, NoPartial));
    }

    // In a constructor...
    // Add Commands:
    // e.g. addSequential(new Command1());
    //      addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    //      addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.


    @Override
    protected void execute()
    {
        if (isFirstCall)
        {
            logger.info("Executing {}", getClass().getSimpleName());
            isFirstCall = false;
        }
        super.execute();
    }


    @Override
    protected void end()
    {
        super.end();
        isFirstCall = true;
    }
}