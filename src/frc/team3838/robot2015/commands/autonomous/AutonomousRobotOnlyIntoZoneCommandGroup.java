package frc.team3838.robot2015.commands.autonomous;


import frc.team3838.robot2015.commands.claw.ClawOpenCommand;
import frc.team3838.robot2015.commands.driveTrain.drive.DriveSetDistance;



public class AutonomousRobotOnlyIntoZoneCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousRobotOnlyIntoZoneCommandGroup() { super(); }

    public AutonomousRobotOnlyIntoZoneCommandGroup(String name) { super(name); }


    @Override
    protected void addCommands()
    {
        //Drive back 6.5 feet
        addSequential(DriveSetDistance.reverseFeet(7.0));
        //Open the claw so we are ready to go for teleop
        addSequential(new ClawOpenCommand());
    }
}