package frc.team3838.robot2015.config;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Port assignments for the Pneumatic Control Module (PCM). [Not to be confused with the PWM (Pulse Width Modulation) class.]
 */
public final class PCM
{
    @Deprecated
    public static final int biDirectionalValue = -1;

    public static final int leftSolenoidA = 0;
    public static final int leftSolenoidB = 1;
    public static final int rightSolenoidA = 2;
    public static final int rightSolenoidB = 3;


    @Deprecated
    public static final int doubleValveForward = -1;
    @Deprecated
    public static final int doubleValveReverse = -1;

    public static final Map<Integer, String> mappings;

    private static final Logger logger = LoggerFactory.getLogger(PCM.class);


    static
    {
        logger.info("Mapping PCM assignments and checking for duplicates");
        mappings = new HashMap<>();
        try
        {
            final Class<PCM> clazz = PCM.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int value = field.getInt(null);
                    if (value != -1)
                    {
                        final String oldValue = mappings.put(value, field.getName());
                        if (oldValue != null)
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : PCM port # %s is duplicated! It is assigned to both '%s' and '%s'", value, oldValue, field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not validate the PCM fields due to an Exception. Cause Details: {}", e.toString(), e);
        }

    }


    private PCM() { }


    public static void init()
    {
        // a no op method to have the PCM class initialize
    }
}
