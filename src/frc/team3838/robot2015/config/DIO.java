package frc.team3838.robot2015.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@SuppressWarnings("PointlessArithmeticExpression")
public class DIO
{
    public static final int TEST_MOTOR_ENCODER_CH_A = -1;
    public static final int TEST_MOTOR_ENCODER_CH_B = -1;

    public static final int WINCH_ENCODER_CH_A = MXP.PORT_ADD_FACTOR + 8;
    public static final int WINCH_ENCODER_CH_B = MXP.PORT_ADD_FACTOR + 9;

    public static final int WINCH_LOWER_LIMIT_SW = 0;
    public static final int WINCH_UPPER_LIMIT_SW = 1;

    public static final int LEES_SWITCH = -1;
    public static final int TEST_LED = -1;


    //A is Blue
    //B is Yellow
    public static final int FRONT_LEFT__WHEEL_ENCODER_CH_A = MXP.PORT_ADD_FACTOR + 1;
    public static final int FRONT_LEFT__WHEEL_ENCODER_CH_B = MXP.PORT_ADD_FACTOR + 0;

    public static final int FRONT_RIGHT_WHEEL_ENCODER_CH_A = MXP.PORT_ADD_FACTOR + 4;
    public static final int FRONT_RIGHT_WHEEL_ENCODER_CH_B = MXP.PORT_ADD_FACTOR + 5;

    public static final int REAR__LEFT__WHEEL_ENCODER_CH_A = MXP.PORT_ADD_FACTOR + 3;
    public static final int REAR__LEFT__WHEEL_ENCODER_CH_B = MXP.PORT_ADD_FACTOR + 2;

    public static final int REAR__RIGHT_WHEEL_ENCODER_CH_A = MXP.PORT_ADD_FACTOR + 6;
    public static final int REAR__RIGHT_WHEEL_ENCODER_CH_B = MXP.PORT_ADD_FACTOR + 7;

    public static final int TEST_DIGITAL_INPUT = MXP.PORT_ADD_FACTOR + 12;


    public static final Map<Integer, String> mappings;

    private static final Logger logger = LoggerFactory.getLogger(DIO.class);


    static
    {
        logger.info("Mapping DIO assignments and checking for duplicates");
        mappings = new HashMap<>();
        try
        {
            final Class<DIO> clazz = DIO.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int value = field.getInt(null);
                    if (value != -1)
                    {
                        final String oldValue = mappings.put(value, field.getName());
                        if (oldValue != null)
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : DIO port # %s is duplicated! It is assigned to both '%s' and '%s'", value, oldValue, field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not validate the DIO fields due to an Exception. Cause Details: {}", e.toString(), e);
        }

    }

    private DIO() { }

    public static void init()
    {
        // a no op method to have the DIO class initialize
    }

}
